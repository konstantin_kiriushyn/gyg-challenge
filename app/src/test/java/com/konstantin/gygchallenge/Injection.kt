package com.konstantin.gygchallenge

import com.konstantin.gygchallenge.data.http.BasicHttpClient
import com.konstantin.gygchallenge.data.repository.ReviewsRepository
import com.konstantin.gygchallenge.data.repository.api.ReviewsApi
import com.konstantin.gygchallenge.data.repository.api.ReviewsNetworkService
import com.konstantin.gygchallenge.data.repository.db.ReviewsDao
import com.squareup.moshi.Moshi
import kotlinx.coroutines.experimental.Unconfined
import org.mockito.Mockito.mock
import kotlin.coroutines.experimental.CoroutineContext

object Injection {
    val reviewsLocalDataSource: ReviewsDao by lazy {
        mock(ReviewsDao::class.java)
    }

    val reviewsRemoteDataSource: ReviewsApi by lazy {
        mock(ReviewsNetworkService::class.java)
    }

    val reviewsRepository: ReviewsRepository by lazy {
        mock(ReviewsRepository::class.java)
    }

    val httpClient: BasicHttpClient by lazy { BasicHttpClient() }

    val moshi: Moshi = Moshi.Builder().build()

    val UI: CoroutineContext = Unconfined
}
