package com.konstantin.gygchallenge.ui.reviews

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.konstantin.gygchallenge.data.http.NetworkException
import com.konstantin.gygchallenge.data.repository.ReviewsRepository
import com.konstantin.gygchallenge.model.KReview
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotSame
import kotlinx.coroutines.experimental.runBlocking
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class ReviewsViewModelTest {

    private val currentCountry = "testCountry"
    private val repository: ReviewsRepository = mock(ReviewsRepository::class.java)
    private val viewModel = ReviewsViewModel(repository, currentCountry)

    @Mock
    lateinit var isLoadingObserver: Observer<Boolean>
    @Mock
    lateinit var reviewsObserver: Observer<List<KReview>>
    @Mock
    lateinit var errorObserver: Observer<String>

    val stubReviewsList = listOf(KReview("1", "1", "1", "1", "1",
            false, "1", "1", "1", "1", "1"),
            KReview("2", "2", "2", "2", "2",
                    false, "2", "2", "2", "2", "2"))

    @get:Rule
    val rule = InstantTaskExecutorRule();

    @Test
    fun `getCurrentFilter() verify that default is NONE`() {
        //GIVEN
        //WHEN
        val filterName = viewModel.getCurrentFilter()
        //THEN
        assertEquals("NONE", filterName)
    }

    @Test
    fun `applyNextFilter() doesn't work without data`() {
        //GIVEN
        //WHEN
        viewModel.applyNextFilter()
        val filterName = viewModel.getCurrentFilter()
        //THEN
        assertEquals("NONE", filterName)
    }


    @Test
    fun `applyNextFilter() changes current filter`() {
        //GIVEN
        viewModel.rawReviewsLiveData.value = emptyList()
        //WHEN
        viewModel.applyNextFilter()
        val filterName = viewModel.getCurrentFilter()
        //THEN
        assertNotSame("NONE", filterName)
    }

    @Test
    fun `loadData() notifies about progress`() {
        //GIVEN
        //we need runBlocking to test coroutines
        runBlocking {
            `when`(repository.loadReviews(0)).thenReturn(stubReviewsList)
        }
        viewModel.isLoadingLiveData.observeForever(isLoadingObserver)
        //WHEN
        viewModel.loadData()
        //THEN
        verify(isLoadingObserver, times(2)).onChanged(ArgumentMatchers.anyBoolean())

    }


    @Test
    fun `loadData() asks repository to load data`() {
        //GIVEN
        //we need runBlocking to test coroutines
        runBlocking {
            `when`(repository.loadReviews(0)).thenReturn(stubReviewsList)
        }
        //WHEN
        viewModel.loadData()
        //THEN
        runBlocking {
            verify(repository).loadReviews(0)
        }
    }

    @Test
    fun `loadData() notifies reviewsLiveData when finished`() {
        //GIVEN
        //we need runBlocking to test coroutines
        runBlocking {
            `when`(repository.loadReviews(0)).thenReturn(stubReviewsList)
        }
        viewModel.reviewsLiveData.observeForever(reviewsObserver)
        //WHEN
        viewModel.loadData()
        //THEN
        verify(reviewsObserver).onChanged(stubReviewsList)
    }

}
