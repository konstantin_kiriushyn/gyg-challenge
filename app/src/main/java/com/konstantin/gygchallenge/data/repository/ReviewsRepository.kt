package com.konstantin.gygchallenge.data.repository

import com.konstantin.gygchallenge.Injection
import com.konstantin.gygchallenge.data.repository.api.ReviewsApi
import com.konstantin.gygchallenge.data.repository.db.ReviewsDao
import com.konstantin.gygchallenge.model.KPostReviewRequest
import com.konstantin.gygchallenge.model.KReview
import com.konstantin.gygchallenge.model.KTravelerType
import kotlinx.coroutines.experimental.async
import java.util.*

/**
 * Repository that uses a network data source (api) and local data source (db)
 * to provide the most recent reviews data.
 */
open class ReviewsRepository(private val api: ReviewsApi,
                             private val db: ReviewsDao) {
    companion object {
        fun getInstance() = ReviewsRepository(Injection.reviewsRemoteDataSource, Injection.reviewsLocalDataSource)
        const val REVIEWS_PER_PAGE = 10
    }

    /**
     *  Load page of reviews. To make it simpler default page size is defined by [REVIEWS_PER_PAGE] constant.
     */
    open suspend fun loadReviews(pageNum: Int): List<KReview> = async {
        val networkResult = api.getReviews(pageNum)

        return@async if (networkResult.isSuccessful) {
            val reviews = networkResult.value!!.data
            //sample logic to show we need update db.
            //Update strategy should be defined in use cases
            db.insert(reviews)
            reviews
        } else {
            //TODO update then db is implemented
            // db.loadReviews()
            throw Exception(networkResult.error)
        }
    }.await()


    /**
     * Submits a review with specified data to the cloud
     */
    suspend fun submitReview(title: String, message: String, rating: Int, travelerType: KTravelerType, reviewDate: Date) {
        val payload = KPostReviewRequest(title, message, travelerType.toString(), rating, reviewDate.time)
        val networkResult = api.postReview(payload)
        // process result
    }
}
