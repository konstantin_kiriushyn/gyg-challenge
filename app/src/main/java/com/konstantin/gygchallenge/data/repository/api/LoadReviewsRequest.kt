package com.konstantin.gygchallenge.data.repository.api

import com.konstantin.gygchallenge.data.http.BaseHttpRequest
import com.konstantin.gygchallenge.data.repository.ReviewsRepository

/**
 * WARNING! Hardcoded request to load page of reviews for a specific tour.
 * TODO change in a future
 */
class LoadReviewsRequest(pageNum: Int) :
        BaseHttpRequest(Method.GET, baseUrl + "&page=${pageNum}") {
    companion object {
        private const val DEFAULT_REGION = "berlin-l17"
        private const val DEFAULT_TOUR = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776"
        val baseUrl = "https://www.getyourguide.com/$DEFAULT_REGION/$DEFAULT_TOUR/reviews.json?count=${ReviewsRepository.REVIEWS_PER_PAGE}"
    }
}
