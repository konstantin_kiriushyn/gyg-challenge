package com.konstantin.gygchallenge.data.repository.db

import com.konstantin.gygchallenge.model.KReview

/**
 * Data Access Object which defines interactions with database
 */
interface ReviewsDao {
    fun loadReviews(): List<KReview>
    fun insert(reviews: List<KReview>)
}
