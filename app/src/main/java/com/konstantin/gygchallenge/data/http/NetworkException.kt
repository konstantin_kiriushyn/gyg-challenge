package com.konstantin.gygchallenge.data.http

/**
 *
 */
class NetworkException(responseMessage: String) : Exception(responseMessage)
