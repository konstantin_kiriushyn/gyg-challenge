package com.konstantin.gygchallenge.data.http

data class RawHttpResponse(var responseCode: Int, var responseMessage: String?)
