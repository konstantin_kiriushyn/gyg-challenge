package com.konstantin.gygchallenge.data.repository.api

import com.konstantin.gygchallenge.Injection
import com.konstantin.gygchallenge.data.http.BasicHttpClient
import com.konstantin.gygchallenge.data.http.NetworkException
import com.konstantin.gygchallenge.data.http.RawHttpResponse
import com.konstantin.gygchallenge.model.KHttpResponse
import com.konstantin.gygchallenge.model.KOperationStatus
import com.konstantin.gygchallenge.model.KPostReviewRequest
import com.konstantin.gygchallenge.model.KReviewsResponse
import com.squareup.moshi.Moshi

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
/**
 *
 */
class ReviewsNetworkService(private val httpClient: BasicHttpClient,
                            private val moshi: Moshi) : ReviewsApi {
    companion object {
        fun getInstance() = ReviewsNetworkService(Injection.httpClient, Injection.moshi)
    }

    override fun getReviews(pageNum: Int): KHttpResponse<KReviewsResponse?> {
        val request = LoadReviewsRequest(pageNum)
        val response = httpClient.execute(request)
        return processResponse(response) {
            moshi.adapter(KReviewsResponse::class.java).fromJson(response.responseMessage)
        }
    }


    //fake response
    override fun postReview(payload: KPostReviewRequest): KHttpResponse<KOperationStatus> =
            KHttpResponse(KOperationStatus(true), null, true)

    /**
     * Check for errors and wrap into a [KHttpResponse]
     */
    private fun <T> processResponse(httpResponse: RawHttpResponse, parse: (String) -> T): KHttpResponse<T> {
        val code = httpResponse.responseCode
        val isSuccessful = !isRequestFailed(code)
        var value: T? = null
        var error: Exception? = null
        if (isSuccessful) {
            value = parse(httpResponse.responseMessage!!)
        } else {
            val errorMessage = httpResponse.responseMessage!!
            error = if (errorMessage.isNotEmpty()) NetworkException(httpResponse.responseMessage!!) else
                NetworkException("Error code $code")
        }
        return KHttpResponse(value, error, isSuccessful)
    }

    private fun isRequestFailed(code: Int) = code >= 300 || code < 200
}
