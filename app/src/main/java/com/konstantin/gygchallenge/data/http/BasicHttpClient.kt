package com.konstantin.gygchallenge.data.http

import android.util.Log
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class BasicHttpClient {

    companion object {
        val TAG: String = BasicHttpClient::class.java.simpleName
        const val READ_TIMEOUT = 3000
        const val CONNECTION_TIMEOUT = 3000
        const val KEY_CONTENT_TYPE = "Content-Type"
        const val KEY_CONTENT_LENGTH = "Content-Length"
    }

    fun execute(request: BaseHttpRequest): RawHttpResponse {
        val url: URL
        var urlConnection: HttpURLConnection? = null
        val response: RawHttpResponse

        try {
            if (request.method == null) {
                throw NetworkException("Malformed BaseHttpRequest")
            }

            url = URL(request.url)
            urlConnection = url.openConnection() as HttpURLConnection
            urlConnection.requestMethod = request.methodName

            //Attach headers
            for (pair in request.headers) {
                urlConnection.setRequestProperty(pair.key, pair.value)
            }

            //set timeouts
            urlConnection.connectTimeout = CONNECTION_TIMEOUT
            urlConnection.readTimeout = READ_TIMEOUT

            //set request body
            attachRequestBody(request, urlConnection)
            //establish connection
            urlConnection.connect()

            //get a response
            response = readResponse(urlConnection)
        } catch (e: Exception) {
            return RawHttpResponse(0, e.message)
        } finally {
            urlConnection?.disconnect()
        }

        return response
    }

    private fun readResponse(urlConnection: HttpURLConnection): RawHttpResponse {
        val responseCode = urlConnection.responseCode
        val responseString = if (isResponseCodeOk(responseCode)) {
            readStream(urlConnection.inputStream)
        } else {
            readStream(urlConnection.errorStream)
        }
        return RawHttpResponse(responseCode, responseString)
    }


    private fun attachRequestBody(request: BaseHttpRequest, urlConnection: HttpURLConnection) {
        if (request.postData != null) {
            val postData = request.postData!!.toByteArray()
            urlConnection.setRequestProperty(KEY_CONTENT_TYPE, "application/json")
            urlConnection.setRequestProperty(KEY_CONTENT_LENGTH, postData.size.toString())
            urlConnection.doOutput = true
            val wr = DataOutputStream(urlConnection.outputStream)
            wr.write(postData)
            wr.close()
        }
    }

    private fun isResponseCodeOk(responseCode: Int) = responseCode == HttpURLConnection.HTTP_OK
            || responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_ACCEPTED

    private fun readStream(stream: InputStream): String {
        var reader: BufferedReader? = null
        val response = StringBuffer()
        try {
            reader = BufferedReader(InputStreamReader(stream))
            var line: String? = null
            while ({ line = reader.readLine(); line }() != null) {
                response.append(line)
            }
        } catch (e: IOException) {
            Log.e(TAG, e.message)
        } finally {
            try {
                reader?.close()
            } catch (e: IOException) {
                Log.e(TAG, e.message)
            }
        }
        return response.toString()
    }
}

