package com.konstantin.gygchallenge.data.http

/**
 * Holds all data needed to execute a correct network request
 */
open class BaseHttpRequest() {
    enum class Method {
        GET,
        POST,
        UPDATE,
        DELETE
    }

    var method: Method? = null
    val methodName: String? by lazy { method?.toString() }
    val headers: HashMap<String, String> by lazy { HashMap<String, String>() }
    var postData: String? = null

    lateinit var url: String

    constructor(method: Method, URL: String, postData: String?) : this() {
        this.method = method
        this.url = URL
        this.postData = postData
    }

    constructor(method: Method, URL: String) : this(method, URL, null)
}
