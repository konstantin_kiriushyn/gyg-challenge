package com.konstantin.gygchallenge.data.repository.api

import com.konstantin.gygchallenge.model.KHttpResponse
import com.konstantin.gygchallenge.model.KOperationStatus
import com.konstantin.gygchallenge.model.KPostReviewRequest
import com.konstantin.gygchallenge.model.KReviewsResponse

/**
 * API communication setup
 */
interface ReviewsApi {
    fun getReviews(pageNum: Int): KHttpResponse<KReviewsResponse?>
    fun postReview(payload: KPostReviewRequest): KHttpResponse<KOperationStatus>
}
