package com.konstantin.gygchallenge

import com.konstantin.gygchallenge.data.http.BasicHttpClient
import com.konstantin.gygchallenge.data.repository.ReviewsRepository
import com.konstantin.gygchallenge.data.repository.api.ReviewsApi
import com.konstantin.gygchallenge.data.repository.api.ReviewsNetworkService
import com.konstantin.gygchallenge.data.repository.db.ReviewsDao
import com.konstantin.gygchallenge.model.KReview
import com.squareup.moshi.Moshi
import kotlin.coroutines.experimental.CoroutineContext

/**
 * Enables injection of production implementations for
 * [ReviewsRepository] at compile time.
 */
object Injection {
    val reviewsLocalDataSource: ReviewsDao by lazy {
        StubReviewDao()
    }

    val reviewsRemoteDataSource: ReviewsApi by lazy {
        ReviewsNetworkService.getInstance()
    }

    val reviewsRepository: ReviewsRepository by lazy {
        ReviewsRepository.getInstance()
    }

    val httpClient: BasicHttpClient by lazy { BasicHttpClient() }

    val moshi: Moshi = Moshi.Builder().build()

    val UI: CoroutineContext = kotlinx.coroutines.experimental.android.UI

    class StubReviewDao : ReviewsDao {
        override fun loadReviews(): List<KReview> = emptyList()

        override fun insert(reviews: List<KReview>) {
            //do nothing at the moment
        }
    }
}
