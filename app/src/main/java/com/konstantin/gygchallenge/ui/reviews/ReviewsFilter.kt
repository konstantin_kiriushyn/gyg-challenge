package com.konstantin.gygchallenge.ui.reviews

import com.konstantin.gygchallenge.model.KReview

/**
 * Set of helper functions
 */
object ReviewsFilter {

    fun ratingEqualsFive(): ((KReview) -> Boolean) = { it.rating.toFloat() == 5f }
    fun ratingThreeOrLower(): ((KReview) -> Boolean) = { it.rating.toFloat() <= 3 }
    fun countryMatchesCurrentLocation(currentCountry: String): ((KReview) -> Boolean) =
            { it.reviewerCountry == currentCountry }

    fun filterList(currentList: List<KReview>, filter: ReviewsViewModel.FilteringType, country: String) =
            when (filter) {
                ReviewsViewModel.FilteringType.NONE -> currentList
                ReviewsViewModel.FilteringType.RATING_PERFECT -> currentList.filter(ratingEqualsFive())
                ReviewsViewModel.FilteringType.RATING_BAD -> currentList.filter(ratingThreeOrLower())
                ReviewsViewModel.FilteringType.COUNTRY -> currentList.filter(countryMatchesCurrentLocation(country))
            }
}
