package com.konstantin.gygchallenge.ui.compose

import android.arch.lifecycle.ViewModel
import com.konstantin.gygchallenge.data.repository.ReviewsRepository
import com.konstantin.gygchallenge.model.KTravelerType
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import java.util.*

/**
 */
class ComposeReviewViewModel(val reviewsRepository: ReviewsRepository) : ViewModel() {

    fun submitReview(title: String, message: String, rating: Int, travelerType: KTravelerType) {
        /*
         * Author, Country, language can be defined by a user session.
         * Date is today. So we need title, message, rating number, and traveler type
         */
        val reviewDate = Date()
        launch(UI) {
            reviewsRepository.submitReview(title, message, rating, travelerType, reviewDate)
        }
    }
}
