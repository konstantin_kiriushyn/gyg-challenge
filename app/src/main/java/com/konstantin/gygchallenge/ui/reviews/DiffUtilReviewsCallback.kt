package com.konstantin.gygchallenge.ui.reviews

import android.support.v7.util.DiffUtil
import com.konstantin.gygchallenge.model.KReview

/**
 * DiffUtil.Callback implementation which help to dynamically update reviews in the adapter
 */
class DiffUtilReviewsCallback(private val oldList: List<KReview>,
                              private val newList: List<KReview>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].review_id == newList[newItemPosition].review_id

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition] == newList[newItemPosition]

}
