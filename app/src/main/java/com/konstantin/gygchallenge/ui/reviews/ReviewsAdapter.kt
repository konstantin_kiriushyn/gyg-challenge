package com.konstantin.gygchallenge.ui.reviews

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.konstantin.gygchallenge.R
import com.konstantin.gygchallenge.model.KReview

/**
 * Displays list of reviews. Takes data from [[ReviewsDataAccessInterface]].
 */
class ReviewsAdapter(private val context: Context,
                     private val dataAccessInterface: ReviewsDataAccessInterface)
    : RecyclerView.Adapter<ReviewsAdapter.ReviewVH>() {

    private val inflater: LayoutInflater by lazy { LayoutInflater.from(context) }
    //use this variable only for DiffUtils
    private var oldList: ArrayList<KReview> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ReviewVH(inflater.inflate(R.layout.item_review, parent, false))

    override fun getItemCount(): Int = dataAccessInterface.getReviews()?.size ?: 0

    override fun getItemId(position: Int): Long {
        val reviews = dataAccessInterface.getReviews()!!
        return reviews[position].review_id.hashCode().toLong()
    }

    override fun onBindViewHolder(holder: ReviewVH, position: Int) {
        val item = dataAccessInterface.getReviews()!![position]
        holder.apply {
            author.text = item.author
            title.text = item.title
            message.text = item.message
            rating.text = context.getString(R.string.label_rating, item.rating)
        }
    }

    fun refreshItems() {
        val newList = dataAccessInterface.getReviews()!!
        val diffResult = DiffUtil.calculateDiff(DiffUtilReviewsCallback(oldList, newList))
        diffResult.dispatchUpdatesTo(this)
        oldList.clear()
        oldList.addAll(newList)
    }

    inner class ReviewVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.txt_item_title)
        val message: TextView = itemView.findViewById(R.id.txt_item_message)
        val rating: TextView = itemView.findViewById(R.id.txt_item_rating)
        val author: TextView = itemView.findViewById(R.id.txt_item_author)
    }
}



