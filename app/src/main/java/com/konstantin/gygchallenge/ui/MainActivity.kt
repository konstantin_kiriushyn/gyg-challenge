package com.konstantin.gygchallenge.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.konstantin.gygchallenge.R
import com.konstantin.gygchallenge.ui.reviews.ReviewsFragment

/**
 * App entry point
 */
class MainActivity : AppCompatActivity() {
    companion object {
        fun <T : ViewModel> obtainViewModel(activity: FragmentActivity, clazz: Class<T>): T {
            // Use a Factory to inject dependencies into the ViewModel
            val factory = ViewModelFactory.getInstance(activity.application)
            return ViewModelProviders.of(activity, factory).get(clazz)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews)

        supportFragmentManager.beginTransaction()
                .replace(R.id.contentFrame, ReviewsFragment.newInstance())
                .commit()
    }
}
