package com.konstantin.gygchallenge.ui.compose

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.konstantin.gygchallenge.R
import com.konstantin.gygchallenge.model.KTravelerType
import com.konstantin.gygchallenge.ui.MainActivity
import kotlin.math.roundToInt

/**
 */
class ComposeReviewFragment : Fragment() {
    companion object {
        val TAG = ComposeReviewFragment::class.java.simpleName!!
    }

    private val viewModel by lazy { MainActivity.obtainViewModel(activity!!, ComposeReviewViewModel::class.java) }
    private lateinit var titleEtxt: EditText
    private lateinit var messageEtxt: EditText
    private lateinit var ratingBar: RatingBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_compose_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.apply {
            val pageTitleTxt = findViewById<TextView>(R.id.txt_title)
            pageTitleTxt.text = getString(R.string.label_let_us_know_what_you_think)
            findViewById<ImageButton>(R.id.btn_filter).visibility = View.GONE
        }

        ratingBar = view.findViewById(R.id.ratingBar)
        titleEtxt = view.findViewById(R.id.etxt_review_title)
        messageEtxt = view.findViewById(R.id.etxt_review_message)


        val submitBtn = view.findViewById<Button>(R.id.btn_submit)
        submitBtn.setOnClickListener {
            val title = titleEtxt.text.toString()
            val message = messageEtxt.text.toString()
            val rating = ratingBar.rating.roundToInt()
            viewModel.submitReview(title, message, rating, KTravelerType.SOLO)
            Toast.makeText(context, "Thanks for submitting. Check ComposeReviewModel for details",
                    Toast.LENGTH_LONG).show()
            activity!!.onBackPressed()
        }
    }
}
