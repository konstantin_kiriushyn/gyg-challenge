package com.konstantin.gygchallenge.ui.reviews

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.konstantin.gygchallenge.R
import com.konstantin.gygchallenge.ui.MainActivity
import com.konstantin.gygchallenge.ui.compose.ComposeReviewFragment

/**
 *
 */
class ReviewsFragment : Fragment() {

    companion object {
        fun newInstance() = ReviewsFragment()
        private const val LOAD_PAGE_OFFSET = 4
    }

    private lateinit var reviewsRv: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var reviewsCountTxt: TextView
    private lateinit var addReviewBtn: FloatingActionButton
    private lateinit var filterBtn: ImageButton

    private lateinit var adapter: ReviewsAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private val viewModel by lazy { MainActivity.obtainViewModel(activity!!, ReviewsViewModel::class.java) }
    private var isLoading = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reviews, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpUI(view)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadData()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.reviewsLiveData.observe(this, Observer {
            adapter.refreshItems()
            reviewsCountTxt.text = getString(R.string.label_reviews_loaded, it!!.size.toString())
        })
        viewModel.errorLiveData.observe(this, Observer {
            Snackbar.make(view!!, it!!, Snackbar.LENGTH_LONG).show()
        })
        viewModel.isLoadingLiveData.observe(this, Observer {
            isLoading = it!!
            progressBar.visibility = if (it) View.VISIBLE else View.GONE
        })
    }

    private fun setUpUI(view: View) {
        activity!!.apply {
            progressBar = findViewById(R.id.pb_main)
            reviewsCountTxt = findViewById(R.id.txt_title)
            filterBtn = findViewById(R.id.btn_filter)
        }
        filterBtn.visibility = View.VISIBLE
        filterBtn.setOnClickListener {
            viewModel.applyNextFilter()
            Toast.makeText(context, getString(R.string.label_current_filter_is, viewModel.getCurrentFilter()),
                    Toast.LENGTH_LONG).show()
        }


        addReviewBtn = view.findViewById(R.id.fab_add_review)
        adapter = ReviewsAdapter(context!!, viewModel)
        adapter.setHasStableIds(true)

        layoutManager = LinearLayoutManager(context)
        reviewsRv = view.findViewById(R.id.recycler_view)
        reviewsRv.let {
            it.layoutManager = layoutManager
            it.adapter = adapter
            it.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    loadMoreItemsOnScroll(dy)
                }
            })
        }

        addReviewBtn.setOnClickListener {
            fragmentManager?.apply {
                beginTransaction()
                        .replace(R.id.contentFrame, ComposeReviewFragment())
                        .addToBackStack(ComposeReviewFragment.TAG)
                        .commit()
            }
        }

    }

    /**
     * Starts loading next page if needed, based on the [layoutManager] scroll data
     */
    private fun loadMoreItemsOnScroll(dy: Int) {
        //check for scroll down
        if (dy > 0) {
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
            if (!isLoading) {
                val isThresholdPassed = visibleItemCount + pastVisiblesItems >= totalItemCount - LOAD_PAGE_OFFSET
                if (isThresholdPassed) {
                    viewModel.loadNextPage()
                }
            }
        }
    }
}
