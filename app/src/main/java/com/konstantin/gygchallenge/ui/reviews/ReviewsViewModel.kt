package com.konstantin.gygchallenge.ui.reviews

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.annotation.VisibleForTesting
import com.konstantin.gygchallenge.Injection
import com.konstantin.gygchallenge.data.repository.ReviewsRepository
import com.konstantin.gygchallenge.model.KReview
import kotlinx.coroutines.experimental.launch
import java.util.*

/**
 * A ViewModel takes care about loading reviews list and applys filters
 */
class ReviewsViewModel(private val reviewsRepository: ReviewsRepository,
                       private val currentCountry: String)
    : ViewModel(), ReviewsDataAccessInterface {

    @VisibleForTesting
    val rawReviewsLiveData: MutableLiveData<List<KReview>> = MutableLiveData()
    val reviewsLiveData: MutableLiveData<List<KReview>> = MutableLiveData()
    val errorLiveData: MutableLiveData<String> = MutableLiveData()
    val isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    private var pageNum: Int = 0

    enum class FilteringType {
        NONE, RATING_PERFECT, RATING_BAD, COUNTRY
    }

    private val filters = FilteringType.values()
    private var currentFilterIndex = 0

    override fun getReviews(): List<KReview>? = reviewsLiveData.value

    fun loadData() {
        isLoadingLiveData.value = true
        launch(Injection.UI) {
            try {
                val reviews = reviewsRepository.loadReviews(pageNum)
                onReviewsLoaded(reviews)
            } catch (e: Exception) {
                onReviewsNotAvailable(e)
            }
        }
    }

    fun loadNextPage() {
        isLoadingLiveData.value = true
        launch(Injection.UI) {
            try {
                val reviews = reviewsRepository.loadReviews(++pageNum)
                onReviewsLoaded(reviews)
            } catch (e: Exception) {
                onReviewsNotAvailable(e)
            }
        }
    }

    private fun onReviewsNotAvailable(e: Exception) {
        isLoadingLiveData.value = false
        errorLiveData.value = e.message
    }

    private fun onReviewsLoaded(reviews: List<KReview>) {
        isLoadingLiveData.value = false
        if (rawReviewsLiveData.value != null) {
            val existingReviews = rawReviewsLiveData.value as ArrayList
            existingReviews.addAll(reviews)
            rawReviewsLiveData.value = existingReviews
        } else {
            rawReviewsLiveData.value = reviews
        }
        showFilteredReviews(filters[currentFilterIndex])
    }

    fun applyNextFilter() {
        if (rawReviewsLiveData.value == null) {
            //Don't apply filter when there is no data
            return
        }
        val totalFiltersCount = filters.size
        //check if it is last array element
        if (currentFilterIndex + 1 == totalFiltersCount) {
            currentFilterIndex = 0
        } else {
            currentFilterIndex++
        }
        val newFilter = filters[currentFilterIndex]
        showFilteredReviews(newFilter)
    }

    private fun showFilteredReviews(filter: FilteringType) {
        reviewsLiveData.value = ReviewsFilter.filterList(rawReviewsLiveData.value!!, filter, currentCountry)
    }


    fun getCurrentFilter() = filters[currentFilterIndex].toString()
}
