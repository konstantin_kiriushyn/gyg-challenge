package com.konstantin.gygchallenge.ui.reviews

import com.konstantin.gygchallenge.model.KReview

/**
 * Interface to get an access to reviews list from the adapter.
 * This way adapter always work with the latest data from the ViewModel
 */
interface ReviewsDataAccessInterface {
    fun getReviews(): List<KReview>?
}
