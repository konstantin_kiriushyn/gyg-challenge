package com.konstantin.gygchallenge.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import com.konstantin.gygchallenge.Injection
import com.konstantin.gygchallenge.data.repository.ReviewsRepository
import com.konstantin.gygchallenge.ui.compose.ComposeReviewViewModel
import com.konstantin.gygchallenge.ui.reviews.ReviewsViewModel

@Suppress("UNCHECKED_CAST")
/**
 * A creator is used to inject the dependencies into the ViewModel
 *
 * This creator is to showcase how to inject dependencies into ViewModels. It's not
 * actually necessary in this implementation, because we can construct ViewModels on place.
 */
class ViewModelFactory private constructor(private val application: Application,
                                           private val reviewsRepository: ReviewsRepository)
    : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ReviewsViewModel::class.java)) {
            return ReviewsViewModel(reviewsRepository, getCurrentCountry()) as T
        } else if (modelClass.isAssignableFrom(ComposeReviewViewModel::class.java)) {
            return ComposeReviewViewModel(reviewsRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    private fun getCurrentCountry() = application.resources.configuration.locale.displayCountry

    companion object {
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application): ViewModelFactory? {

            if (INSTANCE == null) {
                synchronized(ViewModelFactory::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = ViewModelFactory(application, Injection.reviewsRepository)
                    }
                }
            }
            return INSTANCE
        }

        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
