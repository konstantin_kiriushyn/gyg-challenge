package com.konstantin.gygchallenge.model

data class KReview(val review_id: String, val rating: String, val title: String, val message: String,
                   val author: String, val foreignLanguage: Boolean, val date: String, val languageCode: String,
                   val traveler_type: String, val reviewerName: String, val reviewerCountry: String)

enum class KTravelerType(val value: String) {
    SOLO("solo"),
    COUPLE("couple"),
    FAMILY("family")
}

// Data transfer objects

data class KReviewsResponse(val status: Boolean, val totalReviews: Int, val data: List<KReview>)
data class KHttpResponse<out T>(val value: T?, val error: Exception?, val isSuccessful: Boolean)
data class KOperationStatus(val success: Boolean)
data class KPostReviewRequest(val title: String, val message: String, val travelerType: String,
                              val rating: Int, val date: Long)
