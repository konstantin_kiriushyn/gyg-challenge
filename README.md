Implementation uses `kotlin` + `android architecture components` as a base.
I decided to go with [ViewModels](https://developer.android.com/topic/libraries/architecture/viewmodel)+[LiveData](https://developer.android.com/jetpack/arch/livedata) without data bindning instead of presenters. This approach allows to have nice and consistent data flow.
I gave a shot to [kotlin coroutines](https://kotlinlang.org/docs/reference/coroutines.html) to perform tasks asynchronously.
I added some tests to demonstrate my approach.


Potential narrow places:
**Local filtering**. I did filters locally, because I'm not aware of your API capabilities + it was faster.
**Pagination**. Maybe I'n not aware of the whole picture, but I see some potential problems with current pagination architecture. It doesn't use any kind of `id` or `timestamp` which can lead to data duplication on the client side. See [this](https://stackoverflow.com/questions/13872273/api-pagination-best-practices) for details
**Offline scenario**. User can launch the app when he is offline, but there will be no data. Basically, GetYourGuide android app also shows some nice dogs when the app is offline.
It can be fixed with a local storage, but I didn't have time for it.


What I could have done if I had time:
- Add database or local cache
- Add basic local-cloud strategy to repository
- Finish Unit Tests and add UI tests
- UI enhancements
- Extract dimens/styles
